# xivo-sysconfd

[Build](http://jenkins.xivo.solutions:8080/job/xivo-sysconfd/)

xivo-sysconfd is a daemon for configuring system parameters on a XiVO server.
It runs as a server on xivo on localhost port 8668.

## Running unit tests

```bash
pip install -r test-requirements.txt
tox
```
