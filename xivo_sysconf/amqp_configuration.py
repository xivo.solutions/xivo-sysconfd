# -*- coding: utf-8 -*-

# Copyright (C) 2015-2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


import json


class AMQPConfiguration(object):
    def __init__(self, options):
        self._options = options
        self.username = None
        self.password = None
        self.host = None
        self.port = None
        self.exchange_name = None
        self.exchange_type = None
        self.exchange_args = None
        self.exchange_durable = None
        self.url = None
        self.load_configuration()

    def load_configuration(self):
        config = self._options.configuration

        self.username = config.get('bus', 'username')
        self.password = config.get('bus', 'password')
        self.host = config.get('bus', 'host')
        self.port = config.getint('bus', 'port')
        self.exchange_name = config.get('bus', 'exchange_name')
        self.exchange_type = config.get('bus', 'exchange_type')
        exchange_args_string = config.get('bus', 'exchange_args')
        self.exchange_args = json.loads(exchange_args_string)
        self.exchange_durable = config.getboolean('bus', 'exchange_durable')

        self.url = 'amqp://{username}:{password}@{host}:{port}//'.format(username=self.username,
                                                                         password=self.password,
                                                                         host=self.host,
                                                                         port=self.port)
