# -*- coding: utf-8 -*-

# Copyright (C) 2013-2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


import logging

from xivo import debug

from xivo_sysconf.request_handlers.asterisk import AsteriskCommandExecutor, AsteriskCommandFactory

logger = logging.getLogger(__name__)


class OnIpbxHandler(object):

    def __init__(self):
        self._asterisk_command_executor = AsteriskCommandExecutor()
        self._asterisk_command_factory = AsteriskCommandFactory(self._asterisk_command_executor)

    @debug.trace_duration
    def handle_on_ipbx_reload(self, decoded_msg):
        for cmd in decoded_msg:
            exec_command = self._asterisk_command_factory.new_command(cmd)
            exec_command.execute()
