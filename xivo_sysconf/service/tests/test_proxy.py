# -*- coding: utf-8 -*-

# Copyright (C) 2015 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import unittest

from mock import Mock, sentinel as s

from xivo_sysconf.service.handler.on_ipbx import OnIpbxHandler
from xivo_sysconf.service.proxy import ServiceProxy


class TestServiceProxy(unittest.TestCase):

    def setUp(self):
        self.on_ipbx_handler = Mock(OnIpbxHandler)
        self.proxy = ServiceProxy()
        self.proxy.on_ipbx_handler = self.on_ipbx_handler

    def test_on_ipbx_reload(self):
        self.proxy.on_ipbx_reload(s.decoded_msg)

        self.on_ipbx_handler.handle_on_ipbx_reload.assert_called_once_with(s.decoded_msg)
